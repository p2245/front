import {Injectable} from '@angular/core';
import {Observable, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {HttpClient, HttpHeaders, HttpErrorResponse} from '@angular/common/http';
import {Employee} from "../model/Employee";
import {Student} from "../model/Student";

@Injectable({
  providedIn: 'root'
})

export class ApiService {

  baseUri = 'http://localhost:4000/api';
  headers = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private http: HttpClient) {
  }

  // Create
  createEmployee(data): Observable<Student> {
    const url = `${this.baseUri}/create`;
    return this.http.post<Student>(url, data)
      .pipe(
        catchError(this.errorMgmt)
      );
  }

  // Get all employees
  getEmployees(): Observable<any> {
    return this.http.get(`${this.baseUri}`);
  }

  // Get employee
  getEmployee(id): Observable<Student> {
    const url = `${this.baseUri}/read/${id}`;
    return this.http.get(url, {headers: this.headers}).pipe(
      map((res: Student) => {
        return res;
      }),
      catchError(this.errorMgmt)
    );
  }

  // Update employee
  updateEmployee(id, data): Observable<Student> {
    const url = `${this.baseUri}/update/${id}`;
    return this.http.put<Student>(url, data, {headers: this.headers}).pipe(
      catchError(this.errorMgmt)
    );
  }

  // Delete employee
  deleteEmployee(id): Observable<Student> {
    const url = `${this.baseUri}/delete/${id}`;
    return this.http.delete<Student>(url, {headers: this.headers}).pipe(
      catchError(this.errorMgmt)
    );
  }

  // Error handling
  errorMgmt(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }

}
