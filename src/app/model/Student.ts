export class Student {
   name: string;
   grade: string;
   group: string;
   materia: string;
   qualification: string;
}
